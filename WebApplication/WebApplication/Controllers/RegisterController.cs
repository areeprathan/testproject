﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class RegisterController : Controller
    {
        public IActionResult Register()
        {
            return View();
        }
        public async Task <ActionResult> A3(RegistorModel regis)
        {
            
            using (var client = new HttpClient())
            {
                try
                {
                    var DataBody = JsonConvert.SerializeObject(regis);

                    string Token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjFUWUl6czdJeTBKZWhEd2htaEJTenAwU3QyZSIsInNlciI6IlMwMDA4IiwiYXBwIjoiamZ0LXNvbmFyLXNlcnZpY2UiLCJrZXkiOiIxVFlJem42SkdwTERyQlViMkhEc01BUkhrb1UiLCJleHAiOjE2MDUxNjY4NjcsImlhdCI6MTU3MzYzMDg2NywiaXNzIjoiU09OQVIgSU5OT1ZBVElPTiBDTy4sIExURC4ifQ.oc5Qp5z4CBUouEMDy5QZNL8ElTSZ-MtnzEyXbhIqE6LWMd0FS6sVn74PstRfc3nINfCSisnPzJEaDU7Sd4y5Bd8jUry4PjbbBETQM1bwLdj6FKTK74q5MbOnqAlABpXLaSZogi9j_5aJxykZ3kHjNGhpgqvYJv1uV_oHgBSryvA";
                    var BoongServer = "http://f72aa792.ngrok.io/v1/registers_token";               
                    
                    var content = new StringContent(DataBody, Encoding.UTF8, "application/json");

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token); // send header
                    var response = await client.PostAsync(BoongServer, content);
                    var resultToken = await response.Content.ReadAsStringAsync();
                    return Json(content+resultToken);


                }
                catch { 
                
                
                }
                
            }

            return Json(regis);
        }


        //[HttpPost]
        //public async Task<ActionResult> A3(RegisterController regis)
        //{

        //    string Token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjFUWUl6czdJeTBKZWhEd2htaEJTenAwU3QyZSIsInNlciI6IlMwMDA4IiwiYXBwIjoiamZ0LXNvbmFyLXNlcnZpY2UiLCJrZXkiOiIxVFlJem42SkdwTERyQlViMkhEc01BUkhrb1UiLCJleHAiOjE2MDUxNjY4NjcsImlhdCI6MTU3MzYzMDg2NywiaXNzIjoiU09OQVIgSU5OT1ZBVElPTiBDTy4sIExURC4ifQ.oc5Qp5z4CBUouEMDy5QZNL8ElTSZ-MtnzEyXbhIqE6LWMd0FS6sVn74PstRfc3nINfCSisnPzJEaDU7Sd4y5Bd8jUry4PjbbBETQM1bwLdj6FKTK74q5MbOnqAlABpXLaSZogi9j_5aJxykZ3kHjNGhpgqvYJv1uV_oHgBSryvA";


        //    try
        //    {
        //var BoongServer = "https://921a1f22.ngrok.io/v1/registers_token";
        //var client = new HttpClient();
        //var DataBody = JsonConvert.SerializeObject(regis);
        //var content = new StringContent(DataBody, Encoding.UTF8, "application/json");
        //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token); // send header
        //var result = client.PostAsync(BoongServer, content).Result; //send body
        //var response = await client.PostAsync(string.Format(BoongServer), null);
        //var resultToken = await response.Content.ReadAsStringAsync();


        //        return Json(result + resultToken);


        //        //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
        //        //เอา Bearer + Token 
        //        //var response = await client.PostAsync(string.Format(boong), null);
        //        // PostAsync and GetAsync            
        //        //var result = await response.Content.ReadAsStringAsync();
        //        //return Content(result);
        //    }
        //    catch (Exception e)
        //    {
        //        return Content("Catch!!");

        //    }
        //}




    }
}