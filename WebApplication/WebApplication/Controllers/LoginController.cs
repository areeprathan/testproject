﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult test() {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> test_api(login login)
        {

            string prefix = "aaa";
            string Server = "http://f72aa792.ngrok.io/v1/login";
            string Token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjFUWUl6czdJeTBKZWhEd2htaEJTenAwU3QyZSIsInNlciI6IlMwMDA4IiwiYXBwIjoiamZ0LXNvbmFyLXNlcnZpY2UiLCJrZXkiOiIxVFlJem42SkdwTERyQlViMkhEc01BUkhrb1UiLCJleHAiOjE2MDUxNjY4NjcsImlhdCI6MTU3MzYzMDg2NywiaXNzIjoiU09OQVIgSU5OT1ZBVElPTiBDTy4sIExURC4ifQ.oc5Qp5z4CBUouEMDy5QZNL8ElTSZ-MtnzEyXbhIqE6LWMd0FS6sVn74PstRfc3nINfCSisnPzJEaDU7Sd4y5Bd8jUry4PjbbBETQM1bwLdj6FKTK74q5MbOnqAlABpXLaSZogi9j_5aJxykZ3kHjNGhpgqvYJv1uV_oHgBSryvA";
            try
            {
                var path = Server;
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                var json = JsonConvert.SerializeObject(login);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(path, content);
                var result = await response.Content.ReadAsStringAsync();
                return Content(result);
            }
            catch (Exception e)
            {
                return Content("...");
            }

        }
    }
    }
